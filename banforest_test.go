package banforest_test

import (
	"testing"
	banforest "banforest"
)

func TestSliceContains(t *testing.T) {
	tables := []struct {
		sl     []string
		query  string
		result bool
	}{
		{[]string{"foo", "bar", "baz"}, "bar", true},
		{[]string{"foo", "baz"}, "bar", false},
	}

	for _, table := range tables {
		res := banforest.SliceContains(table.sl, table.query)
		if res != table.result {
			t.Errorf("SliceContains was incorrect for (%v,%s) - got: %t, want: %t", table.sl, table.query, res, table.result)
		}
	}
}

func TestSafePop(t *testing.T) {
	tables := []struct {
		sl     []string
		result []string
	}{
		{[]string{"foo", "bar", "baz"}, []string{"bar", "baz"}},
		{[]string{"foo"}, []string{}},
		{[]string{}, []string{}},
	}

	for _, table := range tables {
		res := banforest.SafePop(table.sl)
		if !sliceEquals(res, table.result) {
			t.Errorf("SafePop was incorrect for %v - got: %v, want: %v", table.sl, res, table.result)
		}
	}
}

func sliceEquals(a []string, b []string) bool {
	if len(a) != len(b) {
		return false
	} else {
		for i, v := range a {
			if v != b[i] {
				return false
			}
		}
	}
	return true
}
