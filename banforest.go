package banforest

import (
	"crypto/tls"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	irc "github.com/fluffle/goirc/client"
	"github.com/fluffle/goirc/logging/golog"

	"github.com/davecgh/go-spew/spew"
	log "github.com/fluffle/golog/logging"
	"github.com/tkanos/gonfig"
)

type Configuration struct {
	IrcNick       string
	IrcUser       string
	IrcRealname   string
	IrcAccount    string
	IrcPassword   string
	IrcServer     string
	IrcServerSSL  bool
	IrcServerPort string
	IrcChannels   []string
	IrcBanLimit   int
	IrcBanMargin  float32
}

type BanForest struct {
	Conf      Configuration
	IrcConf   *irc.Config
	IrcClient *irc.Conn
	Bans	  map[string][]string
}

// initialize our logger
var logger = log.NewFromFlags()

func NewForest() *BanForest {
	f := &BanForest{}

	golog.Init()
	f.Conf = Configuration{}
	err := gonfig.GetConf(getFileName(), &f.Conf)
    if err != nil {
        os.Exit(500)
    }

	f.IrcConf = irc.NewConfig(f.Conf.IrcNick)
	f.IrcConf.SSL = f.Conf.IrcServerSSL
	f.IrcConf.SSLConfig = &tls.Config{ServerName: f.Conf.IrcServer}
	serverWithPort := f.Conf.IrcServer +
        ":" + f.Conf.IrcServerPort
	f.IrcConf.Server = serverWithPort
	f.IrcConf.NewNick = func(n string) string { return n + "^" }
	logger.Info("BanForest instantiated. Connecting to %s", serverWithPort)

	f.IrcClient = irc.Client(f.IrcConf)
	f.Bans = make(map[string][]string)

	// do channel joins after we are connected
	f.IrcClient.HandleFunc(irc.CONNECTED,
		func(conn *irc.Conn, line *irc.Line) { doJoins(conn, &f.Conf) })

	// handle end of /names for channel joined detection
	f.IrcClient.HandleFunc("366",
		func(conn *irc.Conn, line *irc.Line) { do366(conn, line, &f.Conf) })

	// handle ban list entries (response to /mode #chan b)
	f.IrcClient.HandleFunc("367",
		func(conn *irc.Conn, line *irc.Line) { do367(conn, line, &f.Conf, f.Bans) })

	// handle MODE changes (we need to stay up to date on banlists..)
	f.IrcClient.HandleFunc(irc.MODE,
		func(conn *irc.Conn, line *irc.Line) { doMode(conn, line, &f.Conf, f.Bans) })

	return f
}

func (f *BanForest) Connect() {
	// quit handler with channel
	quit := make(chan bool)
	f.IrcClient.HandleFunc(irc.DISCONNECTED,
		func(conn *irc.Conn, line *irc.Line) { quit <- true })

	// Tell client to connect.
	if err := f.IrcClient.Connect(); err != nil {
		fmt.Printf("Connection error: %s\n", err.Error())
	}

	// Wait for disconnect
	<-quit
}

// this is called from the CONNECTED handler and joins all configured channels
func doJoins(conn *irc.Conn, conf *Configuration) {
	for _, ch := range conf.IrcChannels {
		conn.Join(ch)
		logger.Info("Joining %s", ch)
	}
}

// this is the MODE handler, it ignores anything but +b/-b - when it sees +b or -b it 
// updates the current banlist in memory with the new information
func doMode(conn *irc.Conn, line *irc.Line, conf *Configuration, bans map[string][]string) {
	//ch := line.Args[1]
	//mode := line.Args[2]
	//logger.Debug("Got mode %s on %s", mode, ch)
	if strings.HasPrefix(line.Args[0], "#") {
		if len(line.Args) > 2 {
			spew.Dump(line)
			add, remove := GetBanChangeLists(line)
			spew.Dump(add, remove)
			ApplyBanlistDiffs(line.Args[0], bans, add, remove)
		}
	}
}

// end of /names handler - schedule a banlist check, time it in the future to avoid
// maxing out our sendq
func do366(conn *irc.Conn, line *irc.Line, conf *Configuration) {
	ch := line.Args[1]
	logger.Debug("Got end of /names for %s", ch)
	spew.Dump(line)
	t := time.NewTimer(10 * time.Second)
	go func() {
		<-t.C
		requestBanlist(conn, ch)
	}()
}

// handle individual banlist entries as received from the server
// add them to the banlist in memory, and if we find a ban in a configured channel that
// starts with $j: , then join the channel it points to
func do367(conn *irc.Conn, line *irc.Line, conf *Configuration, bans map[string][]string) {
	ch := line.Args[1]
	ban := line.Args[2]
	logger.Debug("Got ban %s on %s", ban, ch)
	bans[ch] = append(bans[ch], ban)
	if SliceContains(conf.IrcChannels, ch) {
		// we got a banlist entry for a channel that's in our conf file
		// check for $j: entries and join them
		if strings.HasPrefix(ban, "$j:") {
			newch := strings.SplitAfterN(ban, "$j:", 2)[1]
			logger.Info("Found ban %s on %s - going to join %s", ban, ch, newch)
			conn.Join(newch)
		}
	}
}

//func do368(conn *irc.Conn, line *irc.Line, conf *Configuration, bans map[string][]string) {

// take a channel string, the banlist map, and two lists (add and remove)
// add the bans on the 'add' list to the banlist map for channel
// remove the bans on the 'remove' list from the banlist map for channel
// the list is replaced atomically
func ApplyBanlistDiffs(ch string, bans map[string][]string, add []string, remove []string) {
	spew.Dump(bans)

	newSlice := bans[ch]
	for _, rem := range remove {
		newSlice = SliceRemoveElem(newSlice, rem)
	}

	for _, a := range add {
		newSlice = append(newSlice, a)
	}
	bans[ch] = newSlice
	spew.Dump(bans)
}

// take a mode string e.g. '#foo +b-bb foo!*@* bar!*@* baz!*@*'
// parse it and return two lists, one for adds and one for removes
func GetBanChangeLists(line *irc.Line) ([]string, []string) {
	modestring := line.Args[1]
	targets := line.Args[2:]

	modifier := ""

	add := []string{}
	remove := []string{}

	for _, ru := range modestring {
		char := string(ru)
		switch char {
		case "+", "-":
			modifier = char
		case "b":
			if modifier == "+" {
				add = append(add, targets[0])
				targets = SafePop(targets)
			} else if modifier == "-" {
				remove = append(remove, targets[0])
				targets = SafePop(targets)
			}
		case "q", "e", "I", "o", "v":
			targets = SafePop(targets)
		}
	}
	return add, remove
}

// tell the server that we'd like to see the banlist for ch
func requestBanlist(conn *irc.Conn, ch string) {
	logger.Debug("In requestBanlist for %s", ch)
	conn.Mode(ch, "b")
}

// get filename for configuration file
func getFileName() string {
	env := os.Getenv("ENV")
	if len(env) == 0 {
		env = "development"
	}
	filename := []string{"config.", env, ".json"}
	_, dirname, _, _ := runtime.Caller(0)
	filePath := path.Join(filepath.Dir(dirname), strings.Join(filename, ""))

	return filePath
}

// subtract element r from slice s, return the resulting slice
func SliceRemoveElem(s []string, r string) []string {
	newSlice := []string{}
	for _, a := range s {
		if a != r {
			newSlice = append(newSlice, a)
		}
	}
	return newSlice
}

// if slice s contains string q, return true. Else return false
func SliceContains(s []string, q string) bool {
	for _, a := range s {
		if a == q {
			return true
		}
	}
	return false
}

// return a slice minus its first element. if we get an empty
// slice, then return an empty slice.
func SafePop(sl []string) []string {
	if len(sl) > 1 {
		return sl[1:]
	} else {
		return []string{}
	}
}
